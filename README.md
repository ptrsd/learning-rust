# learning-rust

Exercises from [Rust by Example](https://doc.rust-lang.org/stable/rust-by-example).

### Compile & run

```
rustc 2_1-complex-and-matrix/src/main.rs
./main
```

### Tests

```
cd 2_1-complex-and-matrix
cargo test
```