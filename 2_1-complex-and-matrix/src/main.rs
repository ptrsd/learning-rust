use std::fmt;

#[derive(Debug)]
struct Complex {
    r: f64,
    i: f64,
}

impl fmt::Display for Complex {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{real} {imaginary:+}", real = self.r, imaginary = self.i)
    }
}

#[derive(PartialEq)]
#[derive(Debug)]
struct Matrix(f32, f32, f32, f32);

impl fmt::Display for Matrix {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "( {} {} )\n( {} {} )", self.0, self.1, self.2, self.3)
    }
}

fn transpose(m: Matrix) -> Matrix {
    Matrix(m.0, m.2, m.1, m.3)
}

fn main() {
    let cpl = Complex { r: 3.3, i: 4.4 };
    println!("display: {}", cpl);
    println!("display: {:?}", cpl);

    let matrix = Matrix(1.1, 1.2, 2.1, 2.2);
    println!("Matrix:\n{}", matrix);
    println!("Transpose:\n{}", transpose(matrix));
}

#[test]
fn test_transpose() {
    let input = Matrix(1.1, 1.2, 2.1, 2.2);
    let expected = Matrix(1.1, 2.1, 1.2, 2.2);
    let actual = transpose(input);

    assert_eq!(expected, actual)
}