// A struct with two fields

#[derive(PartialEq)]
#[derive(Debug)]
struct Point {
    x: f32,
    y: f32,
}

impl Point {
    fn square(self, length: f32) -> Rectangle {
        let Point{x: x1, y: y1} = self;
        Rectangle{p1: self, p2: Point{x: x1 + length, y: y1 + length}}
    }
}

#[test]
fn test_square() {
    let input = Point{x: 3.0, y: 0.0};
    let expected = Rectangle{p1: Point{x: input.x, y: input.y}, p2: Point{x: 23.0, y: 20.0}};
    let actual = input.square(20.0);

    assert_eq!(expected, actual)
}

#[derive(PartialEq)]
#[derive(Debug)]
#[allow(dead_code)]
struct Rectangle {
    p1: Point,
    p2: Point,
}

fn rect_area(r: &Rectangle) -> f32 {
    let (&Point { x: x1, y: y1}, &Point{x: x2, y: y2}) = (&r.p1, &r.p2);
    (x2-x1).abs() * (y2-y1).abs()
}

#[test]
fn test_rect_area() {
    let input = Rectangle{ p1: Point{x: 0.0, y: 0.0}, p2: Point{x:2.0, y:-2.0}};
    let expected = 4.0;
    let actual = rect_area(&input);

    assert_eq!(expected, actual)
}